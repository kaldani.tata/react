import React, { useState, useEffect } from 'react';

function App() {
  const [isDarkTheme, setIsDarkTheme] = useState(false);

  const changeTheme = () => {
    setIsDarkTheme((prevTheme) => !prevTheme);
  }

  useEffect(() => {
    const body = document.body;
    const newStyles = {
      backgroundColor: isDarkTheme ? 'black' : 'white',
      color: isDarkTheme ? '#66bb6a' : 'black'
    };
    for (const style in newStyles) {
      body.style[style] = newStyles[style];
    }
  }, [isDarkTheme]);

  const element = React.createElement(
    'h1',
    { style: { color: '#999', fontSize: '19px' } },
    'Solar system planets'
  );

  const planets = (
    <ul className="planets-list">
      <li>Mercury</li>
      <li>Venus</li>
      <li>Earth</li>
      <li>Mars</li>
      <li>Jupiter</li>
      <li>Saturn</li>
      <li>Uranus</li>
      <li>Neptune</li>
    </ul>
  );

  return (
    <div className="App">
      <label className="switch" htmlFor="checkbox">
        <input type="checkbox" id="checkbox" />
        <div className="slider round" onClick={changeTheme}></div>
      </label>
      <div className="homeTask">
        {element}
        {planets}
      </div>
    </div>
  );
}

export default App;
